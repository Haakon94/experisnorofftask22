﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using ClassForTesting;


namespace myTests {
    public class CalculatorTests {
        //[Fact]
        [Theory]
        [InlineData(6, 4, 10)]
        [InlineData(50, 40, 90)]
        public void Add_ShouldFindSumOfNumbers(double num1, double num2, double expected) {
            //Arrange
            // double expected = 10;

            //Act
            double actual = Calculator.Add(num1, num2);

            //Assert
             Assert.Equal(expected, actual);
        }

        //[Fact]
        [Theory]
        [InlineData(6, 2, 4)]
        [InlineData(50, 40, 10)]
        public void Subtract_ShouldFindSumOfSubtraction(double num1, double num2, double expected) {
            //Arrange
            // double expected = 10;

            //Act
             double actual = Calculator.Subtract(num1, num2);

            //Assert
             Assert.Equal(expected, actual);
        }

        //[Fact]
        [Theory]
        [InlineData(6, 4, 24)]
        [InlineData(50, 40, 2000)]
        public void Multiply_ShouldFindProductOfMultiplication(double num1, double num2, double expected) {
            //Arrange
            // double expected = 10;

            //Act
             double actual = Calculator.Multiply(num1, num2);

            //Assert
             Assert.Equal(expected, actual);
        }

        //[Fact]
        [Theory]
        [InlineData(20, 5, 4)]
        [InlineData(15, 3, 5)]
        public void Divide_ShouldFindNumberOfMultiplication(double num1, double num2, double expected) {
            //Arrange
            // double expected = 10;

            //Act
            double actual = Calculator.Divide(num1, num2);

            //Assert
            Assert.Equal(expected, actual);
        }

        //Checking for dividing with zero
        //[Fact]
        [Theory]
        [InlineData(10, 0, false)]
        [InlineData(15, 5, true)]
        public void Divide_IfUserEntersZero(double num1, double num2, bool expected) {
            //Arrange
            //double expected = 10;

            // Act.
            Exception ex = Record.Exception(() => Calculator.Divide(num1, num2));

            // Assert.
            if (!expected) {
                Assert.NotNull(ex);
                Assert.IsType<Exception>(ex);
            } else {
                Assert.Null(ex);
            }
        }

        //Checking for entering first number lower than second
        //[Fact]
        [Theory]
        [InlineData(5, 7, false)]
        [InlineData(15, 5, true)]
        public void Subtract_CheckingForFirstnumberLowerThanSecond(double num1, double num2, bool expected) {
            //Arrange
            //double expected = 10;

            // Act.
            Exception ex = Record.Exception(() => Calculator.Subtract(num1, num2));

            // Assert.
            if (!expected) {
                Assert.NotNull(ex);
                Assert.IsType<Exception>(ex);
            } else {
                Assert.Null(ex);
            }
        }


    }
}
